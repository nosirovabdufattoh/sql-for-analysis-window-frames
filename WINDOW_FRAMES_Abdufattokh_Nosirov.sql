SELECT
  t.time_id,
  EXTRACT(YEAR FROM t.time_id) AS calendar_year,
  EXTRACT(WEEK FROM t.time_id) AS calendar_week_number,
  SUM(s.amount_sold) OVER (ORDER BY t.time_id) AS CUM_SUM,
  AVG(s.amount_sold) OVER (ORDER BY t.time_id ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING) AS CENTERED_3_DAY_AVG
FROM
  sh.times t
JOIN
  sh.sales s ON t.time_id = s.time_id
WHERE
  EXTRACT(WEEK FROM t.time_id) IN (49, 50, 51)
  AND EXTRACT(YEAR FROM t.time_id) = 1999
ORDER BY
  t.time_id;
